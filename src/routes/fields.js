var express = require('express');
var router = express.Router();
var getFields = require('../filler/getFields');

router.post('/', (req, res) => {
  getFields(req.body.formFile, data => {
    res.send({ data });
  });
});

module.exports = router;
