var express = require('express');
var router = express.Router();
var generate = require('../filler/generate');

router.post('/', (req, res) => {
  generate(req.body.formFile, req.body.formData, (buff) => {
    res.contentType('application/pdf');
    res.send(buff);
  });
});

module.exports = router;
