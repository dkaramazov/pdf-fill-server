'use strict';

var hummus = require('hummus');
var PDFDigitalForm = require('./pdf-digital-form');

function getFields(base64, cb) {
  try {
    var buff = Buffer.from(base64, 'base64');
    var file = new hummus.PDFRStreamForBuffer(buff);
    var pdfParser = hummus.createReader(file);
    var digitalForm = new PDFDigitalForm(pdfParser);

    if (digitalForm.hasForm()) {
      return cb(digitalForm.fields);
    }
  } catch (err) {
    console.log(err);
    return null;
  }
}

module.exports = getFields;
