'use strict';

const co = require('co');
const fill = require('@panosoft/pdf-form-fill');

function fillForm(formFile, formData, cb) {
  try {
    co(function*() {
      const input = {
        formFile,
        formData
      };
      cb(yield fill(input));
    });
  } catch(err){
    console.log(err);
    return null;
  }
}

module.exports = fillForm;
